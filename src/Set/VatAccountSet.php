<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\VatAccount\Set;

include_once 'base.php';

/**
 * @property    ORM_StringField     $accountName
 * @property    ORM_DecimalField    $VAT
 * @property    ORM_TextField       $description
 * @property    ORM_BoolField       $disabled
 * @property    ORM_BoolField       $isDefault
 *
 * @method Func_App App()
 */
class VatAccountSet extends \app_TraceableRecordSet
{
    /**
     * 
     * @todo ABA has to do his magic stuff (like crying in the dark) 
     * @param \Func_App $App
     */
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);

        $App = $this->App();
        
        $this->setTableName($App->classPrefix.'VatAccount');
        $this->setDescription($App->translatable('VAT account'));
        
        $this->addFields(
            ORM_StringField('accountName')->setDescription($App->translate('Name')),
            ORM_DecimalField('VAT',2)->setDescription($App->translate('VAT')),
            ORM_TextField('description')->setDescription($App->translate('Description')),
            ORM_BoolField('disabled')->setDescription($App->translate('Disabled account'))->setOutputOptions($App->translate('No'), $App->translate('Yes')),
            ORM_BoolField('isDefault')->setDescription($App->translate('Default VAT account'))->setOutputOptions($App->translate('No'), $App->translate('Yes'))
        );
    }

    public function onUpdate(){
        $App = $this->App();
        $set = $App->VatAccountSet();
        $vatAccounts = $set->select(
            $set->VAT->is(0.00)
        );
        if($vatAccounts->count() == 0){
            $vatAccount = $set->newRecord();
            $vatAccount->VAT = 0.00;
            $vatAccount->accountName= '0%';
            $vatAccount->description = $App->translate('Default VAT account with 0.00%');
            $vatAccount->save();
            $vatAccount->setAsDefaultAccount();
        }
    }
    
    public function getDefaultAccount()
    {
        $set = $this->App()->VatAccountSet();
        return $set->get($set->isDefault->is(true));
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }


    /**
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }

    /**
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }


    /**
     * {@inheritDoc}
     * @see \app_RecordSet::isCreatable()
     */
    public function isCreatable()
    {
        return $this->all();
    }
}

