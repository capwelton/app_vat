<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\VatAccount\Ctrl;

use Capwelton\App\VatAccount\Set\VatAccountSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('VatAccount');


/**
 * This controller manages actions that can be performed on Vat.
 *
 * @method \Func_App App()
 */
class VatAccountController extends \app_ComponentCtrlRecord
{
    
    
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $App->translate('Add VAT Account'),
                $this->proxy()->edit()
            )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        $toolbar->addItem(
            $W->Link(
                $App->translate('Set default VAT account'),
                $this->proxy()->editDefaultVatAccount()
            )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    public function editDefaultVatAccount()
    {
        $App = $this->App();
        $Ui = $App->Ui();
        $W = bab_Widgets();
        $page = $Ui->Page();
        $page->setTitle($App->translate('Set default VAT account'));
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->setName('data');
        $editor->isAjax = true;
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->saveDefaultVatAccount(), $App->translate('Save'));
        
        $set = $App->VatAccountSet();
        $records = $set->select()->orderAsc($set->accountName);
        $options = array();
        foreach ($records as $record){
            $options[$record->id] = $record->display();
        }
        
        
        $editor->addItem(
            $W->LabelledWidget(
                $App->translate('Default VAT account'),
                $select = $W->Select()->setOptions($options),
                'vatAccount'
            )
        );
        
        $currentDefaultVat = $set->get($set->isDefault->is(true));
        if($currentDefaultVat){
            $select->setValue($currentDefaultVat->id);
        }
        
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveDefaultVatAccount($data = null)
    {
        $this->requireSaveMethod();
        
        $App = $this->App();
        $set = $App->VatAccountSet();
        
        $newDefaultVat = $set->get($set->id->is($data['vatAccount']));
        if($newDefaultVat){
            $newDefaultVat->setAsDefaultAccount();
            $this->addMessage($App->translate('The default VAT account has been changed'));
        }
        else{
            $this->addError($App->translate('The selected VAT account was not found'));
        }
        
        $this->addReloadSelector('.depends-vatAccount');
        
        return true;
    }
    
    public function search($q = null){
        $W = bab_Widgets();
        $App = $this->App();
        $set = $App->VatAccountSet();
        
        $W->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
        $counter = 0;
        $criteria = array();
        $criteria[] = $set->any(
            $set->accountName->contains($q),
            $set->VAT->startsWith($q)
        );
        $vats = $set->select(
            $set->all($criteria)
        );
        foreach($vats as $vat){
            $counter++;
            if($counter >10){
                break;
            }
            $collection->addOption(
                new \Widget_Select2Option($vat->id, $vat->display())
            );
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
    }
    
}