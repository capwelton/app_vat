��          �      <      �     �     �  E   �          (     G     S     d     i     l     q  (   �  &   �     �     �     �     �  !  �          *  H   @     �  !   �     �     �     �     �     �  "   �  *     +   C     o  
   s     ~     �                      
                                                   	                            Account name Add VAT Account An already existing VAT Account can't be modified, you can disable it Default VAT account Default VAT account with 0.00% Description Disabled account Name No Save Set default VAT account The default VAT account has been changed The selected VAT account was not found VAT VAT account VAT accounts Yes Project-Id-Version: appvat
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-03-24 12:14+0100
Last-Translator: SI4YOU <contact@si-4you.com>
Language-Team: SI4YOU<contact@si-4you.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: libapp_translate;libapp_translate:1,2;translate:1,2;translate;translatable;translatable:1,2
X-Generator: Poedit 3.0.1
X-Poedit-SearchPath-0: .
 Nom du compte Ajouter un compte TVA Un compte TVA existant ne peux pas être modifié, seulement désactivé Compte TVA par défaut Compte TVA par défaut avec 0.00% Description Compte désactivé Nom Non Enregistrer Définir le compte TVA par défaut Le compte TVA par défaut a été modifié Le compte TVA sélectionné est introuvable TVA Compte TVA Comptes TVA Oui 