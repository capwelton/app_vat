<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\VatAccount\Ui;

use Capwelton\App\VatAccount\Set\VatAccount;

/**
 * @return VatAccountEditor
 */
class VatAccountEditor extends \app_Editor
{
    
    protected $disableInputs = array();
    protected $intro; 
    protected $record;
    
    /**
     * @param \Func_App App
     * @param VatAccount $vatAccount
     * @param int $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, VatAccount $record = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->record = $record;
        parent::__construct($App, $id, $layout);
        $this->addFields();
    }

    protected function addFields()
    {
        $App = $this->App();
        $W = $this->widgets;
        $this->intro = $W->HBoxItems();
        $this->addItem($this->intro);
        
        $box = $W->VBoxItems()->setVerticalSpacing(1, 'em');
        
        $box->addItems(
            $this->accountName(),
            $this->VAT(),
            $this->description(),
            $this->disabled(),
            $this->id()
        )->setVerticalSpacing(1, 'em');
        
        $this->addItem($box);
    }
    
    protected function id(){
        return $this->widgets->Hidden(null,"id",null);
    }
    
    protected function disabled()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Disabled account'),
            $W->CheckBox()->addClass('app-fullwidth'),
            'disabled'
        );
    }
    
    protected function accountName()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Account name'),
            $this->disableInputs[] = $W->LineEdit()->addClass('app-fullwidth')->setSize(50),
            'accountName'
        );
    }
    
    protected function description()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('Description'),
            $W->TextEdit()->addClass('app-fullwidth'),
            'description'
        );
    }
    
    
    
    protected function VAT()
    {
        $W = $this->widgets;
        
        return $this->labelledField(
            $this->App()->translate('VAT'),
            $this->disableInputs[] = $W->LineEdit()->addClass('app-fullwidth')->setSize(50),
            'VAT',
            "%"
        );
    }
    
    
    public function setValues($vatAccount, $namePathBase = array())
    {
        if ($vatAccount instanceof VatAccount) {
            $vatAccountValues = $vatAccount->getValues();
            if($vatAccount->id){
                $this->disableInputs();
            }
            parent::setValues($vatAccountValues, $namePathBase);
        } else {
            if(isset($vatAccount["id"]) && $vatAccount["id"]){
                $this->disableInputs();
            }
            parent::setValues($vatAccount, $namePathBase);
        }
    }
    
    protected function disableInputs(){
        $this->intro->setSizePolicy("alert alert-danger");
        $this->intro->addItem(
            $this->widgets->Label(
                $this->App()->translate("An already existing VAT Account can't be modified, you can disable it")
            )
        );
        foreach($this->disableInputs as $input){
            $input->disable();
        }
        return $this;
    }
}