<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\VatAccount\Ui;

use Capwelton\App\VatAccount\Ctrl\VatAccountController;
use Capwelton\App\VatAccount\Set\VatAccount;
use Capwelton\App\VatAccount\Set\VatAccountSet;

class VatAccountTableView extends \app_TableModelView
{
    protected $widgets;
    
    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        $this->recordController = $App->Controller()->VatAccount();
        $this->widgets = bab_Widgets();
        parent::__construct($App, $id);
        $this->addClass(\Func_Icons::ICON_LEFT_16);
        $this->addClass('depends-vatAccount');
    }
    
    
    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(VatAccountController $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(\ORM_RecordSet $set)
    {
        /* @var $set VatAccountSet */
        $this->addColumn(app_TableModelViewColumn($set->accountName));
        $this->addColumn(app_TableModelViewColumn($set->VAT));
        $this->addColumn(app_TableModelViewColumn($set->description));
        $this->addColumn(app_TableModelViewColumn($set->disabled));
        $this->addColumn(app_TableModelViewColumn($set->isDefault));
    }
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initRow()
     */
    public function initRow(VatAccount $record, $row)
    {
        if($record->disabled){
            $this->addRowClass($row, "alert alert-warning");
        }else{
            $this->addRowClass($row, 'widget-actions-target');
        }
        return parent::initRow($record, $row);
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellTextContent()
     */
    protected function computeCellTextContent(VatAccount $record, $fieldPath)
    {
        switch ($fieldPath) {
            case 'VAT':
                $content = $this->App()->numberFormat($record->VAT)  ."%";
                break;
            default:
                $content = parent::computeCellTextContent($record, $fieldPath);
                break;
        }
        
        return $content;
    }
    
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(VatAccount $record, $fieldPath)
    {
        $W = $this->widgets;        
        $App = $this->App();
        
        switch ($fieldPath) {
            case 'accountName':
                $content = $W->VBoxItems();
                $content->setVerticalSpacing(1,"em");
                
                $content->addItem(
                    $W->Link(
                        $record->accountName,
                        $this->getRecordControllerProxy()->edit($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
                break;
            case 'VAT':
                $content = $W->Label($App->numberFormat($record->VAT)  ."%");
                break;
            default:
                $content = parent::computeCellContent($record, $fieldPath);
                break;
        }
        
        return $content;
    }
    
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();
        
        $conditions = array(
            $recordSet->isReadable()
        );
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->description->contains($filter['search']),
                $recordSet->accountName->contains($filter['search'])
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        
        return $recordSet->all($conditions);
    }
}